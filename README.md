[[_TOC_]]

### At GitLab

I am a [Senior Frontend Engineer](https://about.gitlab.com/job-families/engineering/development/frontend/#senior-frontend-engineer) in the [Release Group](https://about.gitlab.com/handbook/engineering/development/ops/release/) of the DevOps lifecycle.

### About me

My name is Andrei ([pronounced like _/awn-'drey /_](https://app2.nameshouts.com/names/russian/pronounce-andrei)). I am from Elektrostal (which translates to 'Electric steel') - it's a town some 50km from Moscow, Russia. I live in Munich, Germany since 2016. It's a great place with mountains and lakes close by, quiet and sometimes a bit too old-fashioned =).

Before joining Gitlab I worked in Westwing - an E-Commerce company operating in several countries in Europe. I have been working there for a bit over 3 years and progressed from a senior software engineer to a manager of 3 development teams. I was leading the project of re-platforming the legacy monolithic PHP-based UI solution into a micro frontend setup built with node-js and typescript. I've also invested a lot of effort into building the engineering culture that would allow us to deliver on such an ambitious goal, hiring the right people and fostering cross-team connections via internal meetups and guilds. I have some stories to tell, if you're interested just invite me to a coffee chat :) After some years of management, I want to get back to the trenches. To tell the truth, the very short gratification loop that software engineering brings to you is very addictive :)

I wanted to join Gitlab because I strongly believe that remote work and async workflows make people productive while also allowing them to live more fulfilling lives. It's great to have control over your schedule and be able to go to a gym during off-hours to avoid crowds, or catch short winter daylight time and work when it's dark outside anyway. I drew a lot of inspiration from the Gitlab handbook even before joining the company and tried to implement some of the things in my previous company, so after some time I wanted to experience the whole company working in this mode myself.

Another part of wanting to join Gitlab was that I have been an active user of the Gitlab platform for many years and it is a unique chance to be able to work on a product you've been using daily. I am happy that I'm a part of the Release team right now, as that's the area of the product that I have used the most and I want to make it even better!

I am not only working though! Outside of work I probably have too many hobbies, but I just cannot pick one - FOMO is too strong in this one. I love mountains and mountain sports: I am a snowboarder, a boulderer/climber and a hiker. I've recently revived my dormant hobby of scuba diving and am going through some advanced training right now to be able to dive on some fantastic wrecks hidden deep in the dark ocean waters of our plane. I also play guitar (mostly classical and acoustic), watch TV shows and play video games (story-based action games like God of War, all kinds of RPGs, and occasional multiplayer games with my friends). I would like to read more, but cannot bring the habit back. Looks like I can only read either during a commute or on vacation :)

If you ask me about my favorite frontend framework - I don't have one. I believe that as long as a framework can help you build what you want to bring value to your customers, it's good enough. From a framework user perspective - if you know the fundamentals (how JS, browser and web in general work) you can easily learn to use any of the existing ones. I am trying to keep up though and at least form an opinion about strong and week sides of the ones that are currently on the market.

I do have my favorite IDE though :) Or rather a code editor. I like VS Code. I think that's a great product from Microsoft, extensible, responsive and nice. I was using it from the time it went to public beta and participated in the authoring of the first extensions for it. I have to say people building it are amaing, they are very nice and open to the community. I've received a lot of help and support from them.

#### Presence on the web

- [LinkedIn](https://www.linkedin.com/in/andreizubov/)
- [Twitter](https://twitter.com/ReflectionDM)

### Personal traits

Here are some facts about me to give you an idea of who (I think) I am:
In learning, I have a bias towards doing. If that's not available, reading is better than a video.
- I want to make the world a better place, be it on code, application or process level. I am pragmatic here though - it is about the result, not the process, and the benefits must overweight the costs
-
### Collaboration

Here are some things to take into account when collaborating with me:

- I am always eager to offer help. Ask me anything anytime.
- That being said, I have bias for problem solving. I find it hard to tell if someone is asking for help or just sharing frustration. Please, consider making it clear to me
- I am happy to have some face time, so feel free to reach out for a coffee chat, pair programming session, or anything!

